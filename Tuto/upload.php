<?php
require 'kint/Kint.class.php';
require 'db.php';
$success = false;
$error = false;

$db = db::getInstance('base_upload');
$connection = $db->getConnection();

if (isset($_POST['bad-upload'])) {
    $destRep = "uploads/";
    $destFile = $destRep.basename($_FILES['file']['name']);
    $imageFileType = pathinfo($destFile, PATHINFO_EXTENSION);
    if ($_FILES['file']['size'] > 1*1024*1024) {
        $error = "Fichier trop gros";
    }
    if (!$error && !in_array($imageFileType, ['jpg', 'jpeg', 'png', 'gif'])) {
        $error = "Type dez fichier non supporté";
    }
    if (!$error) {
        move_uploaded_file($_FILES['file']['tmp_name'], $destFile);
        $success = "Upload successfull !";
    }
}

if (isset($_POST['good-upload'])) {
    $destRep = "C:\\gallery/";
    $baseFile = basename($_FILES['file']['name']);
    $uploadName = sha1($baseFile.microtime().uniqid());
    $imageFileType = pathinfo($baseFile, PATHINFO_EXTENSION);
    $contentType = 'image/'.$imageFileType;

    if ($_FILES['file']['size'] > 1*1024*1024) {
        $error = "Fichier trop gros";
    }
    if (!$error && !in_array($imageFileType, ['jpg', 'jpeg', 'png', 'gif'])) {
        $error = "Type dez fichier non supporté";
    }

    if (!$error) {
        $sum = md5_file($_FILES['file']['tmp_name']);
        $dest = $destRep.$uploadName;
        $pstmt = $db->makePstmt('INSERT INTO file(filename, contenttype, path, sum) values (?,?,?,?)');
        $pstmt->bind_param('ssss', $baseFile, $contentType, $dest, $sum);
        $pstmt->execute();
        $pstmt->close();
        move_uploaded_file($_FILES['file']['tmp_name'], $destRep.$uploadName);
        $success = "Upload successfull !";
    }

}

if (isset($_GET['fileid'])) {
    $fileId = $_GET['fileid'];
    $fileselect = "select * from file where id = ".$fileId;
    $result = mysqli_query($connection, $fileselect);
    $foundFile = mysqli_fetch_object($result);

    if (md5_file($foundFile->path) != $foundFile->sum) {
        $error = "Sum doesn't match, file is corrupted";
    }
    if (!$error) {
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for i.e.
        header("Content-Type: ".$foundFile->contenttype);
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:".filesize($foundFile->path));
        header("Content-Disposition: attachment; filename=".$foundFile->filename);
        readfile($foundFile->path);
        die();
    }
}

$filesSelect = "SELECT * FROM file";
$files = mysqli_query($connection, $filesSelect);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Un moteur de templating risqué</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/extra.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey darken-1">
    <?php if ($success): ?>
        <div class="row">
            <div class="col s12 center-align green lighten-3 white-text z-depth-3">
                <h1><?php echo $success; ?></h1>
            </div>
        </div>
    <?php elseif($error): ?>
        <div class="row">
            <div class="col s12 center-align red lighten-3 white-text z-depth-3">
                <h1><?php echo $error; ?></h1>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
            <dov class="col s12">
              <h3>Upload insecure :</h3>
            </dov>
          </div>
          <div class="row">
                <div class="file-field input-field">
                    <div class="btn red">
                        <span>File</span>
                        <input type="file" name="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
               </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="bad-upload">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
            <dov class="col s12">
              <h3>Upload secure :</h3>
            </dov>
          </div>
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn blue">
                        <span>File</span>
                        <input type="file"  name="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
               </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="good-upload">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <fiv class="row">
        <div class="col s8 offset-s2 white z-depth-3">
            <table>
                <thead>
                    <tr>
                        <th>File</th>
                        <th>Preview</th>
                        <th>Download</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($file = mysqli_fetch_object($files)): ?>
                    <tr>
                        <td>
                            <?php echo $file->filename; ?>
                        </td>
                        <td>
                            <img src="/tuto/upload.php?fileid=<?php echo $file->id; ?>" width="80"/>
                        </td>
                        <td>
                            <form class="" action="" method="get">
                                <input type="hidden" name="fileid" value="<?php echo $file->id; ?>">
                                <button class="btn waves-effect waves-light" type="submit" name="download">Download
                                  <i class="material-icons right">play_for_work</i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </fiv>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('select').material_select();
    });
    </script>
  </body>
</html>
