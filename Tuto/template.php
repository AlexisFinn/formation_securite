<?php
require 'kint/Kint.class.php';
$firstName = 'alexis';
if (isset($_POST['template'])) {
    $template = $_POST['template'];
    $search = '/\[([^\]]+)\]/e'; // je cherche les champs entre crochets pour evaluation
    $replace = "\$\\1"; // remplacer le contenu des crochets par le contenu entre les crochets + un $ devant
    $render = preg_replace($search, $replace, $template);

}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Un moteur de templating risqué</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey lighten-1">
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>Here goes template :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="template" value="" class="validate">
                <label for="template">This is my template</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <?php if (isset($render)): ?>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
          <?php echo $render; ?>
      </div>
    </div>
    <?php endif; ?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
