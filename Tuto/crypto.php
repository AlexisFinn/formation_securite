<?php
if (isset($_GET['value'])) {
    $entry = $_GET['value'];
    $iv= '12312312';
    $pass = 'admin';
    $result = encrypt($entry, $pass, $iv);
    echo $entry;
    echo '<br/>';
    echo $result;
    echo '<br/>';
    echo decrypt($result, $pass, $iv);
}

function encrypt($message, $pass, $iv) {
    $enc = mcrypt_encrypt(MCRYPT_BLOWFISH, $pass, $message, MCRYPT_MODE_CBC, $iv);

    return bin2hex($enc);
}

function decrypt($crypted, $pass, $iv) {
    $crypted = hex2bin($crypted);
    return mcrypt_decrypt(MCRYPT_BLOWFISH, $pass,$crypted, MCRYPT_MODE_CBC, $iv);

}
?>
