<?php

function safe($value) {
  // return htmlentities($value);
  // return htmlentities($value, ENT_NOQUOTES);
  // return htmlentities($value, ENT_COMPAT);
  return strip_tags($value, "<b><ul><li>");// remove all tags except for Bold and list.
}

if (isset($_POST['comment'])) {
  $comment = $_POST['comment'];
  $title = safe($comment);
} else {
  $title = 'no comment';
}
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Poster votre commentaire</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
      <div class="row">
        <div class="col s10 offset-s1">
          <form class="" action="comment.php" method="post">
            <div class="card green darken-3">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $title; ?></span>
                <div class="row">
                  <div class="input-field col s8">
                    <textarea name="comment" rows="5" cols="50"></textarea>
                  </div>
                </div>
                <div class="card-actions">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                      <i class="material-icons right">send</i>
                    </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
  </body>
</html>
