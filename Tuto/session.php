<?php

function generateUrl($sessionId, $url) {
    return $url.'?PHPSESSID='.$sessionId;
}

require 'SessionObject.php';
ini_set('session.use_only_cookies', 0);
$session = new SessionObject();
$compteur = $session->get('compteur');
if (null !== $compteur) {
    $session->set('compteur', $compteur + 1);
} else {
    $session->set('compteur', 0);
}

if (isset($_POST['nosession'])) {
    $lien = generateUrl($_POST['nosession'], $_POST['url']);
    echo '<div class="row">
        <div class="col s6">
            <h2>Lien a cliquer</h2>
        </div>
    </div>';
    echo '<div class="col s3"><a href="'.$lien.'" class="btn">cliquez moi !</a></div>';
}

 ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/extra.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
  <body>
      <form action="" method="post">
        <div class="row">
          <dov class="col s12">
            <h3>Fix your session</h3>
          </dov>
        </div>
          <div class="row">
            <div class="input-field col s12">
              <input type="text" name="url" value="" class="validate">
              <label for="url">UrlSite</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <label for="nosession">NoSession</label>
              <textarea name="nosession" rows="5" cols="50" class="materialize-textarea"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col s2">
              <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
      </form>

      <div class="row">
          <div class="col s2 offset-s10">
              Compteur: <span class="blue white-text badge"><?php echo $session->get('compteur'); ?></span>
          </div>
      </div>
      <div class="row">
          <div class="col s2 offset-s10">
              Session Id: <span class="blue white-text badge"><?php echo $session->getId(); ?></span>
          </div>
      </div>

      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
