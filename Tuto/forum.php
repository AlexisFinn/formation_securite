<?php
require 'db.php';

function safe($value) {
  $value = strip_tags($value, "<b><ul><li>");
  $value = str_replace('"', '&quot;', $value);
  $value = str_replace("'", '&apos;', $value);
  return $value;
}
function escape($value) {
    return htmlentities($value);
}


$db = db::getInstance('base_xss');
$connection = $db->getConnection();
$pstmt = $db->makePstmt('INSERT INTO forum_post (titre, corps) values (?, ?)');

if (isset($_POST['comment']) && isset($_POST['title'])) {
  $title = escape($_POST['title']);
  $comment = safe($_POST['comment']);
  $pstmt->bind_param('ss', $title, $comment);
  $pstmt->execute();
  $pstmt->close();

}

$select = "SELECT * FROM forum_post ORDER BY id DESC";
$result = mysqli_query($connection, $select);

 ?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Un simple forum</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey darken-1">
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="forum.php" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>Add your own post</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="title" value="" class="validate">
                <label for="title">Titre</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <label for="title">Message</label>
                <textarea name="comment" rows="5" cols="50" class="materialize-textarea"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <?php while ($currentPost = mysqli_fetch_object($result)): ?>
      <div class="row">
        <div class="col s8 offset-s2 light-blue white-text">
          <div class="row teal lighten-1">
            <div class="col s12 z-depth-4">
              <h3 class="center-align"><?php echo $currentPost->titre; ?></h3>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <p class="flow-text">
                <?php echo safe($currentPost->corps); ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
