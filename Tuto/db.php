<?php

/**
 *
 */
class Db
{
  private $connection;
  private static $instance;

  public static function getInstance($database)
  {
    if (!self::$instance) {
        self::$instance = new Db($database);
    }

    return self::$instance;
  }

  private function __construct($database)
  {
    $this->connection = mysqli_connect("localhost", "root", "", $database);
  }

  public function getConnection()
  {
      return $this->connection;
  }

  public function escape($value)
  {
      return mysql_real_escape_string($value);
  }

  public function makePstmt($query)
  {
      return mysqli_prepare($this->getConnection(), $query);
  }

}
