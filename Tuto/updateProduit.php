<?php
require_once 'db.php';

$db = db::getInstance();

$connect = $db->getConnection();

if (!$connect) exit ("connect fail");

$produitId = $db->escape($_POST['id']);
$nouveauPrix = $db->escape($_POST['prix']);
$nouveauNom = $db->escape($_POST['nom']);
$nouveauPoids = $db->escape($_POST['poids']);

$stmt = $db->makePstmt("update produit set prix = ?, nom = ?, poids= ? where id = ?");

mysqli_stmt_bind_param($stmt, "ssdd", $nouveauPrix, $nouveauNom, $nouveauPoids, $produitId);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $result);

header('Location: index.php');
?>
