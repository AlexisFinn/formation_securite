<?php

/**
 *
 */
class SessionObject
{

    function __construct()
    {
        session_start();
    }

    public function get($param)
    {
        return isset($_SESSION[$param])?$_SESSION[$param]:null;
    }

    public function set($param, $value)
    {
        $_SESSION[$param] = $value;
    }

    public function getId()
    {
        return session_id();
    }
}


 ?>
