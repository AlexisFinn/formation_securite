<?php
require  'db.php';
require 'kint/Kint.class.php';
$db = db::getInstance('base_crypto');
$conn = $db->getConnection();
$stmtInsert = $db->makePstmt("INSERT INTO utilisateurs(login, password, hash, salt) values (?, ?, ?, ?)");
$stmtSelectHash = $db->makePstmt("SELECT hash, salt FROM utilisateurs WHERE login = ?");
$stmtConnect = $db->makePstmt("SELECT login FROM utilisateurs WHERE password = ?");
$success = false;

if (!empty($_POST['login-connect']) && !empty($_POST['password-connect'])) {
    $loginC = $_POST['login-connect'];
    $passwordC = $_POST['password-connect'];
    $stmtSelectHash->bind_param('s', $loginC);
    $stmtSelectHash->execute();
    $stmtSelectHash->bind_result($hash, $salt);
    $stmtSelectHash->fetch();
    $stmtSelectHash->close();
    if ($hash) {
        if ($hash == 'sha1') {
            $testPassword = sha1($passwordC.$salt);
        } else {
            $testPassword = md5($passwordC.$salt);
        }
        $stmtConnect->bind_param('s', $testPassword);
        $stmtConnect->execute();
        $stmtConnect->bind_result($loggedUser);
        $stmtConnect->fetch();
        $stmtConnect->close();
        if ($loggedUser) {
            $success = $loggedUser;
        } else {
            d('YOU NO GOOD PASSWORD !');
        }
    } else {
        d('YOU NO EXIST !');
    }
}

if (isset($_POST['login']) && isset($_POST['password']) && isset($_POST['hash'])) {
    $login = $_POST['login'];
    $psswd = $_POST['password'];
    $hash = $_POST['hash'];
    $salt = uniqid();
    if ($hash == 'sha1') {
        $psswd = sha1($psswd.$salt);
        $stmtInsert->bind_param('ssss', $login, $psswd, $hash, $salt);

    } else {
        $psswd = md5($psswd.$salt);
        $stmtInsert->bind_param('ssss', $login, $psswd, $hash, $salt);
    }

    $stmtInsert->execute();
}

$select = "SELECT * FROM utilisateurs";
$result = mysqli_query($conn, $select);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Un moteur de templating risqué</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey lighten-1">
    <?php if ($success): ?>
        <div class="row">
            <div class="col s12 center-align green lighten-3 white-text z-depth-3">
                <h1><?php echo $success; ?> IS LOGGED IN SUCCESSFULLY ! </h1>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>Login :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="login-connect" value="" class="validate">
                <label for="login-connect">Login</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="password-connect" value="" class="validate">
                <label for="password-connect">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>New user :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="login" value="" class="validate">
                <label for="login">Login</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6">
                <input type="password" name="password" value="" class="validate">
                <label for="password">Password</label>
              </div>
              <div class="input-field col s6">
                  <select name="hash">
                      <option value="md5" selected="selected">md5</option>
                      <option value="sha1">sha1</option>
                  </select>
                  <label>Hash method</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <?php while ($user = mysqli_fetch_object($result)): ?>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
          <table>
              <thead>
                  <tr>
                      <th>
                          hash
                      </th>
                      <th>
                          login
                      </th>
                      <th>
                          pass
                      </th>
                      <th>
                          salt
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>
                          <?php echo $user->hash ?>
                      </td>
                      <td>
                          <?php echo $user->login ?>
                      </td>
                      <td>
                          <?php echo $user->password ?>
                      </td>
                      <td>
                          <?php echo $user->salt; ?>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
    </div>
    <?php endwhile; ?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('select').material_select();
    });
    </script>
  </body>
</html>
