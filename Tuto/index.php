<?php
$connect = mysqli_connect("localhost", "root", "", "base_injection");
$query = "select * from produit";
$result = mysqli_query($connect, $query);

?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page de test</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
  <?php while ($produit = mysqli_fetch_object($result)) : ?>
  <div class="row">
    <form class="col s8 offset-s2" action="updateProduit.php" method="post">
      <input type="hidden" name="id" value="<?php echo $produit->id; ?>">
        <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Produit</span>
          <div class="row">
            <div class="input-field col s4">
              <input name="nom" placeholder="nom" id="nom" type="text" class="validate" value="<?php echo $produit->nom; ?>">
              <label for="nom">Nom</label>
            </div>
            <div class="input-field col s4">
              <input name="prix" id="prix" type="text" class="validate" value="<?php echo $produit->prix; ?>">
              <label for="prix">Prix</label>
            </div>
            <div class="input-field col s4">
              <input value="<?php echo $produit->poids; ?>" id="poids" type="text" name="poids" class="validate">
              <label for="poids">Poids</label>
            </div>
          </div>
          <div class="card-actions">
              <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="divider"></div>
  <div class="row">
    <div class="col s8 offset-s2">
      <div class="row">
        <div class="col s3">
          <a href="collectCookie.php" class="btn">Tracking on click</a>
        </div>
        <div class="col s3">
          <a href="" onmouseover="window.location=collectCookie.php">Tracking on mouseover</a>
        </div>
        <div class="col s3">
          <img src="collectCookie.php" title="image tracking (on page load)"/>
        </div>
        <div class="col s3">
          <table background="collectCookie.php">
          </table>
        </div>
      </div>
      <!-- <div style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;" class="blue lighten-1"></div> -->
    </div>
  </div>
  <?php endwhile; ?>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
