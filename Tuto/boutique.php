<?php
require 'kint/Kint.class.php';
require 'db.php';
session_start();
$success = false;
$error = false;
$files = [];
$users = [];
$products = [];

$db = db::getInstance('base_injection');
$connection = $db->getConnection();
/// UTILISATEURS ****************************************
$stmtInsert = $db->makePstmt("INSERT INTO utilisateurs(login, password, hash, salt) values (?, ?, ?, ?)");
$stmtSelectHash = $db->makePstmt("SELECT hash, salt FROM utilisateurs WHERE login = ?");
$stmtConnect = $db->makePstmt("SELECT id, login FROM utilisateurs WHERE password = ?");


if (!empty($_POST['login-connect']) && !empty($_POST['password-connect'])) {
    $loginC = $_POST['login-connect'];
    $passwordC = $_POST['password-connect'];
    $stmtSelectHash->bind_param('s', $loginC);
    $stmtSelectHash->execute();
    $stmtSelectHash->bind_result($hash, $salt);
    $stmtSelectHash->fetch();
    $stmtSelectHash->close();
    if ($hash) {
        if ($hash == 'sha1') {
            $testPassword = sha1($passwordC.$salt);
        } else {
            $testPassword = md5($passwordC.$salt);
        }
        $stmtConnect->bind_param('s', $testPassword);
        $stmtConnect->execute();
        $stmtConnect->bind_result($userId, $loggedUser);
        $stmtConnect->fetch();
        $stmtConnect->close();
        if ($loggedUser) {
            $success = "Success login in as $loggedUser";
            $_SESSION['current_user_name'] = $loggedUser;
            $_SESSION['current_user_id'] = $userId;
        } else {
            $error = 'YOU NO GOOD PASSWORD !';
        }
    } else {
        $error = 'YOU NO EXIST !';
    }
}

if (isset($_POST['login']) && isset($_POST['password']) && isset($_POST['hash'])) {
    $login = $_POST['login'];
    $psswd = $_POST['password'];
    $hash = $_POST['hash'];
    $salt = uniqid();
    if ($hash == 'sha1') {
        $psswd = sha1($psswd.$salt);
        $stmtInsert->bind_param('ssss', $login, $psswd, $hash, $salt);

    } else {
        $psswd = md5($psswd.$salt);
        $stmtInsert->bind_param('ssss', $login, $psswd, $hash, $salt);
    }

    $stmtInsert->execute();
}
/// IMAGES ****************************************

if (isset($_POST['good-upload'])) {
    $destRep = "C:\\gallery/";
    $baseFile = basename($_FILES['file']['name']);
    $uploadName = sha1($baseFile.microtime().uniqid());
    $imageFileType = pathinfo($baseFile, PATHINFO_EXTENSION);
    $contentType = 'image/'.$imageFileType;

    if ($_FILES['file']['size'] > 1*1024*1024) {
        $error = "Fichier trop gros";
    }
    if (!$error && !in_array($imageFileType, ['jpg', 'jpeg', 'png', 'gif'])) {
        $error = "Type dez fichier non supporté";
    }

    if (!$error) {
        $sum = md5_file($_FILES['file']['tmp_name']);
        $dest = $destRep.$uploadName;
        $pstmt = $db->makePstmt('INSERT INTO image(filename, contenttype, path, sum) values (?,?,?,?)');
        $pstmt->bind_param('ssss', $baseFile, $contentType, $dest, $sum);
        $pstmt->execute();
        $pstmt->close();
        move_uploaded_file($_FILES['file']['tmp_name'], $destRep.$uploadName);
        $success = "Upload successfull !";
    }

}

if (isset($_GET['fileid'])) {
    $fileId = $_GET['fileid'];
    $fileselect = "select * from image where id = ".$fileId;
    $result = mysqli_query($connection, $fileselect);
    $foundFile = mysqli_fetch_object($result);

    if (md5_file($foundFile->path) != $foundFile->sum) {
        $error = "Sum doesn't match, file is corrupted";
    }
    if (!$error) {
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for i.e.
        header("Content-Type: ".$foundFile->contenttype);
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:".filesize($foundFile->path));
        header("Content-Disposition: attachment; filename=".$foundFile->filename);
        readfile($foundFile->path);
        die();
    }
}

if (isset($_POST['pUpdate'])) {
    $nom = $connection->escape_string($_POST['pName']);
    $prix = $_POST['pPrice'];
    $poids = $_POST['pPoids'];
    $image = $_POST['pImage'];
    $pId = $_POST['pId'];
    $query = sprintf('UPDATE produit set nom = \'%s\', prix = %d, poids = %d, image_id = %d where id = %d', $nom, $prix, $poids, $image, $pId);
    mysqli_query($connection, $query);
}

if (isset($_POST['pCreate'])) {
    $pstmt = $db->makePstmt('INSERT INTO produit (nom, prix, poids, image_id, proprietaireId) values (?,?,?,?,?)');
    $nom = $_POST['pName'];
    $prix = $_POST['pPrice'];
    $poids = $_POST['pPoids'];
    $image = $_POST['pImage'];
    $userId = $_POST['pUser'];

    $pstmt->bind_param('sdddd', $nom, $prix, $poids, $image, $userId);
    $pstmt->execute();
    $pstmt->close();
}

$filesSelect = "SELECT * FROM image";
$usersSelect = "SELECT * FROM utilisateurs";
$productsSelect = "SELECT * FROM produit";
$filesResult = mysqli_query($connection, $filesSelect);
$usersResult = mysqli_query($connection, $usersSelect);
$productsResult = mysqli_query($connection, $productsSelect);
while ($file = mysqli_fetch_object($filesResult)) {
    $files[] = $file;
}
while ($product = mysqli_fetch_object($productsResult)) {
    $products[] = $product;
}
while ($user = mysqli_fetch_object($usersResult)) {
    $users[] = $user;
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Ma petite boutique</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/extra.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey darken-1">
    <?php if ($success): ?>
        <div class="row">
            <div class="col s12 center-align green lighten-3 white-text z-depth-3">
                <h1><?php echo $success; ?></h1>
            </div>
        </div>
    <?php elseif($error): ?>
        <div class="row">
            <div class="col s12 center-align red lighten-3 white-text z-depth-3">
                <h1><?php echo $error; ?></h1>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>Login :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="login-connect" value="" class="validate">
                <label for="login-connect">Login</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="password-connect" value="" class="validate">
                <label for="password-connect">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
      <?php if (isset($_SESSION['current_user'])): ?>
          <div class="col s2 blue darken-2 white-text z-depth-1"><h5><?php echo $_SESSION['current_user']; ?><i class="material-icons right">perm_identity</i></h5></div>
      <?php endif; ?>
    </div>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>New user :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="login" value="" class="validate">
                <label for="login">Login</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6">
                <input type="password" name="password" value="" class="validate">
                <label for="password">Password</label>
              </div>
              <div class="input-field col s6">
                  <select name="hash">
                      <option value="md5" selected="selected">md5</option>
                      <option value="sha1">sha1</option>
                  </select>
                  <label>Hash method</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <?php foreach ($users as $user): ?>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
          <table>
              <thead>
                  <tr>
                      <th>hash</th>
                      <th>login</th>
                      <th>pass</th>
                      <th>salt</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td><?php echo $user->hash ?></td>
                      <td><?php echo $user->login ?></td>
                      <td><?php echo $user->password ?></td>
                      <td><?php echo $user->salt; ?></td>
                  </tr>
              </tbody>
          </table>
      </div>
    </div>
    <?php endforeach; ?>
    <div class="row">
        <div class="col s12 black">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
            <dov class="col s12">
              <h3>Upload image :</h3>
            </dov>
          </div>
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn blue">
                        <span>File</span>
                        <input type="file"  name="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
               </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="good-upload">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <div class="row">
        <div class="col s8 offset-s2 white z-depth-3">
            <table>
                <thead>
                    <tr>
                        <th>File</th>
                        <th>Preview</th>
                        <th>Download</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($files as $file): ?>
                    <tr>
                        <td><?php echo $file->filename; ?></td>
                        <td><img src="/tuto/boutique.php?fileid=<?php echo $file->id; ?>" width="80"/></td>
                        <td>
                            <form class="" action="" method="get">
                                <input type="hidden" name="fileid" value="<?php echo $file->id; ?>">
                                <button class="btn waves-effect waves-light" type="submit" name="download">Download
                                  <i class="material-icons right">play_for_work</i>
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col s12 black">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>New product :</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s6">
                <input type="text" name="pName" value="" class="validate">
                <label for="login">Nom</label>
              </div>
              <div class="input-field col s6">
                  <select name="pImage">
                      <?php foreach ($files as $file): ?>
                          <option value="<?php echo $file->id; ?>"><?php echo $file->filename; ?></option>
                      <?php endforeach; ?>
                  </select>
                  <label>Image</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s3">
                <input type="text" name="pPrice" value="" class="validate">
                <i class="material-icons prefix">$</i>
                <label for="pPrice">Prix</label>
              </div>
              <div class="input-field col s3">
                <input type="text" name="pPoids" value="" class="validate">
                <i class="material-icons prefix">G</i>
                <label for="pPoids">Poids</label>
              </div>
              <div class="input-field col s4 offset-s2">
                  <select name="pUser">
                      <?php foreach ($users as $user): ?>
                          <option value="<?php echo $user->id; ?>"><?php echo $user->login; ?></option>
                      <?php endforeach; ?>
                  </select>
                  <label>Utilisateur</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="pCreate">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
    <div class="row">
        <div class="col s8 offset-s2 white z-depth-3">
            <table>
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prix</th>
                        <th>Poids</th>
                        <th>image</th>
                        <th>
                            actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product): ?>
                        <?php if (isset($_SESSION['current_user_id']) && $_SESSION['current_user_id'] == $product->proprietaireId ): ?>
                            <form class="" action="" method="post">
                                <input type="hidden" name="pId" value="<?php echo $product->id; ?>" />
                                <tr class="teal lighten-3">
                                    <td>
                                        <input type="text" name="pName" value="<?php echo $product->nom; ?>" class="validate">
                                    </td>
                                    <td><input type="text" name="pPrice" value="<?php echo $product->prix; ?>" class="validate"></td>
                                    <td><input type="text" name="pPoids" value="<?php echo $product->poids; ?>" class="validate"></td>
                                    <td>
                                        <select name="pImage">
                                        <?php foreach ($files as $file): ?>
                                            <option value="<?php echo $file->id; ?>"><?php echo $file->filename; ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                        <label><img src="/tuto/boutique.php?fileid=<?php echo $product->image_id; ?>" width="0"/></label>
                                    </td>
                                    <td>
                                        <button class="btn waves-effect waves-light" type="submit" name="pUpdate">Submit
                                          <i class="material-icons right">send</i>
                                        </button>
                                    </td>
                                </tr>
                            </form>
                        <?php else: ?>
                        <tr>
                            <td><?php echo $product->nom; ?></td>
                            <td><?php echo $product->prix; ?></td>
                            <td><?php echo $product->poids; ?></td>
                            <td><img src="/tuto/boutique.php?fileid=<?php echo $product->image_id; ?>" width="80"/></td>
                            <td>

                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('select').material_select();
    });
    </script>
  </body>
</html>
