<?php
require 'cup.php';
require 'mug.php';
require 'kint/Kint.class.php';

function safe_for_eval($string) {
    $nl = chr(10);
    if (strrpos($string, $nl) !== false) {
        exit("$string is not permitted");
    }

    $out = addslashes($string);
    $meta = [';', '$']; //...
    $escaped = ['&#59', '&#36']; //...
    $out = str_replace($meta, $escaped, $out);

    return $out;
}

if (isset($_POST['iwant'])) {
    $type = $_POST['iwant'];
    $safeType = safe_for_eval($type);
    eval("\$iwant = new $safeType();");
    d($iwant);
}

 ?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Un mauvais trip eval</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body class="grey darken-1">
    <div class="row">
      <div class="col s8 offset-s2 white z-depth-3">
        <form action="" method="post">
          <div class="row">
            <dov class="col s12">
              <h3>What do you want ?</h3>
            </dov>
          </div>
            <div class="row">
              <div class="input-field col s12">
                <input type="text" name="iwant" value="" class="validate">
                <label for="iwant">This is what I want</label>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
