<?php
require_once 'db.php';

$db = db::getInstance('base_xss');

$conn = $db->getConnection();

$stmt = $db->makePstmt("INSERT INTO linkcollect (provenance) values (?)");

if ($stmt) {
  $stmt->bind_param('s', $_SERVER['REQUEST_URI']);
  $stmt->execute();
  $stmt->close();
}
 ?>
