<?php

// 100.0 where id=3 -- 
// 100.0 where 1=1 -- 
// 250.0, nom=(SELECT password FROM client where id=1) where id=3 -- 
	require("produit_metier.php");

	@ $Db = new mysqli('localhost', 'root', '', 'base_injection');
	if (mysqli_connect_errno()) {
		echo "erreur connection base";
		exit;
	}
	
	if (isset($_POST["action"])) {
		$action = $_POST["action"];
	}
	else{
		$action = "none";
	}
	
	if ($action == "sauver") {
		$np = new PRODUIT($_POST["id"], "", $_POST["prix"], 0, false);
		$stmt = $Db->prepare('UPDATE Produit set prix=? where id=?');
		$stmt->bind_param("di", $np->prix, $np->id);
		$result = $stmt->execute();
		if ($result) {
			echo $result . " produits mis à jours <br/>";
		}
		// code suivant (en commentaire) vulnérable aux injections SQL
		
		/*$np = new PRODUIT($_POST["id"], "", $_POST["prix"], 0, false);
		$requete = 'UPDATE Produit set prix=' . $np->prix . " where id=" . $np->id;
		$result = $Db->query($requete);
		if ($result) {
			echo $result . " produits mis à jours <br/>";
		}*/
	}
	
	$listrequete = "select * from Produit";
	$resultlist = $Db->query($listrequete);
	$nbligne = $resultlist->num_rows;
	
	echo "<table border='1'>";
	for ($i = 0; $i < $nbligne; $i++) {
		$np = new PRODUIT(0, "", "", 0, false);
		$np->remplir_depuis_base($resultlist->fetch_assoc());
		echo $np->affiche_produit();
	}
	echo "</table>";
	$resultlist->close();
	$Db->close();
?>