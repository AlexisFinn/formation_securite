<?php
if (isset($_GET["id"])) {
	@ $Db = new Mysqli('localhost', 'root', '', 'base_upload');
		if (mysqli_connect_errno()) {
		echo 'erreur de connection';
		exit;
	}
	$stmt = $Db->prepare(
		"select fileName, contentType, storageName, sommeControle from imageFiles where id=?");
	$stmt->bind_param("i",$_GET["id"]);
	$stmt->execute();
	$stmt->bind_result($fileName, $contentType, $storageName, $sommeControle);
	$stmt->fetch();
	if (!isset($storageName)) {
		echo "image inconnue";
		exit;
	}
	$gallerieRep = "c:\\gallerie/";
	if (!file_exists($gallerieRep . $storageName)) {
		echo "fichier introuvable";
		exit;		
	}
	if (sha1_file($gallerieRep . $storageName) != $sommeControle) {
		echo "fichier corrompu";
		exit;				
	}
	header('Content-Type: ' . $contentType);
	header('Content-Disposition: attachment; filename="' . $fileName . '"');
	readfile($gallerieRep . $storageName);
}
?>