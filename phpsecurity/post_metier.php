<?php

class POST
{
	var $id;
	var $titre;
	var $corps;
	
	
	public function __construct($vid, $vtitre, $vcorps) {
		$this->id = $vid;
		$this->titre = $vtitre;
		$this->corps = $vcorps;
	}
	
	public function affiche_post() {
		$div = "<div><h2>";
		$div .= safe_without_format_value($this->titre); // echappement des entite pour le titre
		$div .= "</h2><p>";
		$div .= safe_with_format_value($this->corps);
		$div .= "</p></div>";
		return $div;
	}
	
	public function fill_from_db($ligne) {
		$this->id = $ligne["id"];
		$this->titre = $ligne["titre"];
		$this->corps = $ligne["corps"];
	}
	
	public function fill_from_form($form) {
		//$this->id = $form["id"];
		$this->titre = safe_without_format_value($form["titre"]);
		$this->corps = safe_with_format_value($form["corps"]);
	}
	
	public function save_to_db($Db) {
		$stmt = $Db->prepare("insert into Post (titre, corps) VALUES (?, ?)");
		$stmt->bind_param("ss", $this->titre, $this->corps);
		$stmt->execute();
		$stmt->close();
	}
	
	
	
}

?>