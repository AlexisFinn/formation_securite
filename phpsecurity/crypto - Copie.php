<?php
$message = "un super secret que personne ne doit connaitre";
$iv =  '12345678'; // bloc "initiateur" de la chaine de crypto
$passphrase = "8silhs687";

function encryptString($message, $passphrase, $iv) {
	$enc = mcrypt_encrypt(MCRYPT_BLOWFISH, $passphrase, $message, MCRYPT_MODE_CBC, $iv);
	return base64_encode($enc);
}

function decryptString($encoded, $passphrase, $iv) {
	$string = base64_decode($encoded);
	$dec = mcrypt_decrypt(MCRYPT_BLOWFISH, $passphrase, $string, MCRYPT_MODE_CBC, $iv);
	return $dec;
}

$encryptedMessage = encryptString($message, $passphrase, $iv);
$decryptedMessage = decryptString($encryptedMessage, $passphrase, $iv);

echo "<pre>" . $encryptedMessage . "</pre>";
echo "<pre>" . $decryptedMessage . "</pre>";


?>