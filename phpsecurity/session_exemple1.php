<?php

// forcer l'utilisation des cookies
ini_set('session.use_only_cookies', 0);
// définir l'expiration d'un cookies
ini_set('session.cookie_lifetime', 600);


session_start();

session_regenerate_id(); //protection partielle contre la fixation de session
// régénere automatiquement l'identifiant de session sans détruire le contenu
// de celle-ci


if (isset($_SESSION['compteur'])) {
	$_SESSION['compteur'] = $_SESSION['compteur'] + 1;
}
else {
	$_SESSION['compteur'] = 1;
}

echo "identifiant de la session = " .session_id() . " <br />";
echo "compteur = " . $_SESSION['compteur'] . " <br />";

?>

<a href="session_exemple1.php">rafraichir la page</a>
