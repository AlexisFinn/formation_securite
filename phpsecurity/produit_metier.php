<?php

class PRODUIT
{
	var $id;
	var $nom;
	var	$prix;
	var $poids;
	var $editable;
	
	public function __construct($vid, $vnom, $vprix, $vpoids, $veditable) {
		$this->id = $vid;
		$this->nom = $vnom;
		$this->prix = $vprix;
		$this->poids = $vpoids;
		$this->editable = $veditable;
	}
	
	public function saisie_edit($action) {
		$form = '<form action="boutique.php" method="post">';
		$form .= '<input type="hidden" name="id" value="' . $this->id . '"/>';
		$form .= '<input type="hidden" name="action" value="sauver"/>';
		$form .= 'prix : <input type="text" name="prix" value="' . $this->prix . '"/>';
		$form .= '<input type="submit" />';
		$form .= '</form>';
		return $form;
	}
	
	public function remplir_depuis_base($ligne) {
		$this->id = $ligne["id"];
		$this->nom = $ligne["nom"];
		$this->prix = $ligne["prix"];
		$this->poids = $ligne["poids"];
		$this->editable = $ligne["editable"];
	}
	
	public function affiche_produit() {
		$ligne = "<tr><td>" . $this->id . "</td>";
		$ligne .= "<td>" . $this->nom . "</td>";
		$ligne .= "<td>" . $this->prix . "</td>";
		$ligne .= "<td>" . $this->poids . "</td>";
		$ligne .= "<td>" . $this->editable . "</td>";
		if ($this->editable) {
			$ligne .= '<td>' . $this->saisie_edit() . "</td>";	
		}
		else {
			$ligne .= '<td>non editable</td>';	
		}
		return $ligne . "</tr>";
	}
	
	public function saisie_add() {
		$form = '<form action="boutique.php" method="post">';
		$form .= '<input type="hidden" name="action" value="sauver"/>';
		$form .= 'prix : <input type="text" name="prix" value="' . $this->prix . '"/>';
		$form .= '<input type="submit" />';
		$form .= '</form>';
		return $form;
	}
}

?>