
<?php

$connect = mysqli_connect("localhost", "root", "", "base_injection");
if (!$connect) exit ('connection failed');

$nom = $_POST["nom"];
$mdp = $_POST["mdp"];

// ne jamais JAMAIS faire comme cela, pour illustration
// si on injecte dans nom : "nom\" -- ", le login fonctionnera sans mot de passe
$query = "SELECT * from client WHERE nom=\"$nom\" AND password=\"$mdp\"";
echo $query . "<br />";
$result = mysqli_query($connect, $query);
// probleme avec la requete
if (!$result) {
	echo "login failed, no info";
}
// aucune ligne
else if (mysqli_num_rows($result) < 1) {
	echo "login failed";
}
// plusieurs lignes
else if (mysqli_num_rows($result) > 1) {
	echo "probleme, plusieurs lignes renvoyées";
}
// 1 ligne renvoyée
else {
	echo "login successfull";
}

?>