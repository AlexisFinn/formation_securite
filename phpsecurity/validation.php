<?php

// expected nous dira ce que l'on veu récupérer
// dans le tableau $values
function copyParams($expected, $values) {
		$params = array();
		foreach( $expected AS $key => $value) {
			if (empty($values[$key])) {
				continue;
			}
			switch($value) {
				case 'string':
					if (is_string($values[$key]) && strlen($values[$key]) < 256 ) {
						$params[$key] = $values[$key];
					}
					break;
				case 'number':
					if (is_numeric($values[$key]))
						$params[$key] = (int)$values[$key];
					break;
				case 'filename':
					if (is_string($values[$key]) && strlen($values[$key]) < 64) {
						$temp = str_replace('%', '_', rawurlencode($values[$key]));
						echo strpos($temp, '..') . "<br />";
						if (strpos($temp, '..') !== false) {
							$params[$key] = NUll;
						}
						else {
							$params[$key] = $temp;
						}
					}
					break;
			}
			//$params[$key] = $values[$key];
		}
		
		return $params;
}

$expected =  array('id' => 'number', 'nom' => 'string', 'imageFile' => 'filename');
$params = copyParams($expected, $_GET);

var_dump($params);

if (isset($params["isAdmin"]) && $params["isAdmin"]) {
	echo "interface administrateur";
}
else {
	echo "interface normale";
}

/*$isAdmin = false; // on imagine un code de vérification de l'utilisateur

foreach( $_GET as $key => $value) {
	${$key} = $value;
}


if ($isAdmin == TRUE) {
	echo "interface administrateur";
}
else {
	echo "interface normale";
}*/
?>