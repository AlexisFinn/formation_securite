<html>
	<head><title>le café php</title></head>
	<body>
	<form method="post" action="evalphp.php">
		que voulez vous : <input type="text" name="type" />
		<input type="submit" />
	</form>
<?php

class TASSE {
	var $content;
	
	function __construct() {
		$this->content = "café";
	}
}

class MUG {
	var $content;
	
	function __construct() {
		$this->content = "chocolat";
	}
}

function safe_for_eval($string) {
	$nl = chr(10);
	if (strpos($string, $nl) !== false) {
		exit("$string is not permitted");
	}
	$out = addslashes($string); // insertion de '\' devans les '"etc...
	$meta = array(';', '$', '{', '}', '[', ']', '`');
	$escaped = array('&#59', '&#36', '&#123', '&#125', '&#91', '&#93', '&#96');
	$out = str_replace($meta, $escaped, $out); 
	return $out;
}

if (isset($_POST["type"])) {
	$type = $_POST['type'];
	$safetype = safe_for_eval($type);
	
	//$command = "\$objet = new $type;";
	$command = "\$objet = new $safetype;";
	
	eval($command); // attention! non sécurisé
	echo "<pre> commande! $type  nettoyé : $safetype</pre>";
	echo "<pre> votre " . get_class($objet) . " contient " . $objet->content . "</pre>";
}
	?>
	<a href="evalphp.php">retour a la page</a>
	
	</body>
</html>