<?php

$mysqli = new mysqli('localhost', 'root', '', 'base_xss');
if (!$mysqli) exit('connection failed');

$stmt = $mysqli->prepare("INSERT INTO linkCollect (provenance, dateHit) VALUES (?, ?)");
if ($stmt) {
	$date = "" . Date('Y-m-d H:i:s');
	$stmt->bind_param('ss', $_SERVER['REQUEST_URI'], $date);
	$stmt->execute();
	$stmt->close();
}
else {
	echo "problem with query";
}
$mysqli->close();

?>