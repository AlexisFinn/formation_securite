-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 20 Avril 2016 à 09:24
-- Version du serveur :  10.1.10-MariaDB
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `base_crypto`
--
DROP DATABASE `base_crypto`;
CREATE DATABASE IF NOT EXISTS `base_crypto` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `base_crypto`;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `salt` varchar(50) DEFAULT NULL,
  `hash` varchar(10) NOT NULL DEFAULT 'md5'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `login`, `password`, `salt`, `hash`) VALUES
(1, 'alexis', '2439feb5d9da4cbb531c427a9b2542aff6808372', '5716432a95799', 'sha1'),
(2, 'toto', '379c24c3acdd4558eb14e7311641e392', '571647a69a4ee', 'md5');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;--
-- Base de données :  `base_injection`
--
DROP DATABASE `base_injection`;
CREATE DATABASE IF NOT EXISTS `base_injection` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `base_injection`;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `password`, `actif`) VALUES
(1, 'bruce', 'batman', 1),
(2, 'clark', 'superman', 1);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prix` varchar(11) NOT NULL,
  `poids` int(11) DEFAULT NULL,
  `editable` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `prix`, `poids`, `editable`) VALUES
(1, 'Galaxy S6', '689', 12, 1),
(2, 'Serviette de bain', '40', 10, 1),
(3, 'BatMobile', '1200000', 800, 1),
(4, 'batman', '12', 0, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;--
-- Base de données :  `base_xss`
--
DROP DATABASE `base_xss`;
CREATE DATABASE IF NOT EXISTS `base_xss` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `base_xss`;

-- --------------------------------------------------------

--
-- Structure de la table `forum_post`
--

CREATE TABLE `forum_post` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `corps` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `forum_post`
--

INSERT INTO `forum_post` (`id`, `titre`, `corps`) VALUES
(4, 'Cecie est un titre &lt;b&gt;AVEC&lt;/b&gt; des balises', 'Je met <b>aussi</b> des <ul><li>balises<li><li>dans</li></ul>dans le corps'),
(5, 'Le titre', 'sur\r\nplusieurs\r\nlignes'),
(8, 'track', '<b onclick="window.location=''collectCookie.php''">click here</b>'),
(10, 'try 2', '</b>\r\n<ul onmouseover="xhttp= new  XMLHttpRequest();  xhttp.open(''GET'', ''collectCookie.php'', true); xhttp.send();" >\r\n<li>toto 2 </li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Structure de la table `linkcollect`
--

CREATE TABLE `linkcollect` (
  `id` int(11) NOT NULL,
  `provenance` varchar(100) NOT NULL,
  `dateHit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `linkcollect`
--

INSERT INTO `linkcollect` (`id`, `provenance`, `dateHit`) VALUES
(1, '/tuto/collectCookie.php', '0000-00-00 00:00:00'),
(2, '/tuto/collectCookie.php', '2016-04-19 07:59:39'),
(3, '/tuto/collectCookie.php', '2016-04-19 09:04:31'),
(4, '/tuto/collectCookie.php', '2016-04-19 09:09:17'),
(5, '/tuto/collectCookie.php', '2016-04-19 09:09:22'),
(6, '/tuto/collectCookie.php', '2016-04-19 09:09:23'),
(7, '/tuto/collectCookie.php', '2016-04-19 09:09:27'),
(8, '/tuto/collectCookie.php', '2016-04-19 09:09:29'),
(9, '/tuto/collectCookie.php', '2016-04-19 09:09:30'),
(10, '/tuto/collectCookie.php', '2016-04-19 09:09:34'),
(11, '/tuto/collectCookie.php', '2016-04-19 09:09:34'),
(12, '/tuto/collectCookie.php', '2016-04-19 09:09:35'),
(13, '/tuto/collectCookie.php', '2016-04-19 09:09:35'),
(14, '/tuto/collectCookie.php', '2016-04-19 09:09:35'),
(15, '/tuto/collectCookie.php', '2016-04-19 09:09:35'),
(16, '/tuto/collectCookie.php', '2016-04-19 09:09:35'),
(17, '/tuto/collectCookie.php', '2016-04-19 09:09:36'),
(18, '/tuto/collectCookie.php', '2016-04-19 09:09:36'),
(19, '/tuto/collectCookie.php', '2016-04-19 09:09:36'),
(20, '/tuto/collectCookie.php', '2016-04-19 09:09:36'),
(21, '/tuto/collectCookie.php', '2016-04-19 09:09:37'),
(22, '/tuto/collectCookie.php', '2016-04-19 09:09:37'),
(23, '/tuto/collectCookie.php', '2016-04-19 09:09:37'),
(24, '/tuto/collectCookie.php', '2016-04-19 09:09:37'),
(25, '/tuto/collectCookie.php', '2016-04-19 09:09:37'),
(26, '/tuto/collectCookie.php', '2016-04-19 09:09:38'),
(27, '/tuto/collectCookie.php', '2016-04-19 09:30:41'),
(28, '/tuto/collectCookie.php', '2016-04-19 09:30:41'),
(29, '/tuto/collectCookie.php', '2016-04-19 09:35:23'),
(30, '/tuto/collectCookie.php', '2016-04-19 09:35:25'),
(31, '/tuto/collectCookie.php', '2016-04-19 09:35:27'),
(32, '/tuto/collectCookie.php', '2016-04-19 09:36:33'),
(33, '/tuto/collectCookie.php', '2016-04-19 09:36:43'),
(34, '/tuto/collectCookie.php', '2016-04-19 09:36:46'),
(35, '/tuto/collectCookie.php', '2016-04-19 09:36:47');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `forum_post`
--
ALTER TABLE `forum_post`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `linkcollect`
--
ALTER TABLE `linkcollect`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `forum_post`
--
ALTER TABLE `forum_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `linkcollect`
--
ALTER TABLE `linkcollect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
